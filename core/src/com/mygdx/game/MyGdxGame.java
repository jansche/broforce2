package com.mygdx.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Screens.PlayScreen;

import java.io.File;

public class MyGdxGame extends Game {

	public static SpriteBatch batch;
	//Player player;
	//Foe foeone;
	//Sound shootSound;
	//Music backgroundMusic;

	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new PlayScreen(this));
		//player = new Player(200, 200);
		//foeone = new Foe(200,200);
		//shootSound = Gdx.audio.newSound(Gdx.files.internal("shootSound.mp3"));
		//backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("1 - Strident - Broforce Theme Song.mp3"));
		//backgroundMusic.play();
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
