package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Player;
import com.badlogic.gdx.utils.viewport.Viewport;

public class PlayScreen implements Screen {

    private MyGdxGame game;

    private OrthographicCamera gameCam;
    private Viewport viewPort;

    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    private World world;
    private Body body;

    Player player;

    public PlayScreen(MyGdxGame game) {

        this.game = game;

        gameCam = new OrthographicCamera();
        gameCam.setToOrtho(false);
        viewPort = new ScreenViewport(gameCam);
        ((ScreenViewport) viewPort).setUnitsPerPixel(0.6f);
        //viewPort.setWorldWidth(7680);
        viewPort.setWorldHeight(310);
        //System.out.println(viewPort.getWorldWidth());
        //viewPort.setScreenPosition((int)viewPort.getWorldWidth(), (int)viewPort.getWorldHeight());
        mapLoader = new TmxMapLoader();
        map = mapLoader.load("Map1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);
        player = new Player(Gdx.graphics.getWidth() / 2,80);
        gameCam.position.y = viewPort.getWorldHeight() / 2 + 50;

        //Box2d
        //world = new World(Vector2(0,0), );
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor( 0, 0, 0, 0 );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

        if (player.getX() < 700) {
            player.setPosition(700, player.getY());
        } else if (player.getX() > 3599) {
            player.setPosition(3600, player.getY());
        }
        if (player.getY() < 0) {
            player.setPosition(player.getX(), 0);
        } else if (player.getY() > 259) {
            player.setPosition(player.getX(), 260);
        }



        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            player.translate(-4, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            player.translate(4, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            player.translate(0, 4);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            player.translate(0, -4);
        }

        if (Gdx.graphics.getHeight() > 685) {
            Gdx.graphics.setWindowedMode(Gdx.graphics.getWidth(), 685);
        }

        gameCam.position.x = player.getX() + viewPort.getWorldHeight() /2 - 100;
        gameCam.update();
        renderer.setView(gameCam);
        renderer.render();

        game.batch.setProjectionMatrix(gameCam.combined);
        game.batch.begin();
        player.draw(game.batch);
        game.batch.end();


    }

    @Override
    public void resize(int width, int height) {
        viewPort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
