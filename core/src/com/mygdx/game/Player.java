package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Player{

    Sprite playerSprite;

    public Player(int x, int y) {
        playerSprite = new Sprite(new Texture("player.png"));
        playerSprite.setPosition(x, y);
    }

    public void draw(SpriteBatch batch) {
        playerSprite.draw(batch);
    }

    public void translate(int x, int y) {
        playerSprite.translate(x, y);
    }

    public int getX() {
        return (int) playerSprite.getX();
    }

    public void setPosition(int x, int y) {
        playerSprite.setPosition(x,y);
    }

    public int getY() {
        return (int) playerSprite.getY();
    }
}
