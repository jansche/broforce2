package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Foe {

    Sprite foesprite;

    public Foe(int x, int y) {
        foesprite = new Sprite(new Texture("foe.png"));
        foesprite.setPosition(x, y);
    }

    public void draw(SpriteBatch batch) {
        foesprite.draw(batch);
    }

    public void translate(int x, int y) {
        foesprite.translate(x, y);
    }
}
