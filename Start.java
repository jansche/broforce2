
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class Start extends JFrame implements ActionListener {

    private JButton schliessen;
    private JButton einstellungen;
    private JButton info;
    private JButton ende;




    public static void main(String[] args) {
        Start frame = new frame("Menü");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);
        frame.setLayout(null);
        frame.setVisible(true);



    }

    public Start (String title){
        super(title);

        schliessen = new JButton("Spiel starten");
        schliessen.setBounds(120,40,160,40 );
        schliessen.addActionListener(this);
        add(schliessen);

        einstellungen = new JButton("Spiel starten");
        einstellungen.setBounds(120,120,160,40 );
        einstellungen.addActionListener(this);
        add(einstellungen);

        info = new JButton("Spiel starten");
        info.setBounds(120,200,160,40 );
        info.addActionListener(this);
        add(info);

        ende = new JButton("Spiel starten");
        ende.setBounds(120,280,160,40 );
        ende.addActionListener(this);
        add(ende);

    }



    public void ActionPerformed(ActionEvent e){

        if (e.getSource()== schliessen){
            fenster();
        }

        if(e.getSource()== info){
            Object[] options = {"OK"};

            JOptionPane.showOptionDialog(null,"Programmiert von Jan Hagen und Christoph Faißt","Informationen",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE,null,options,options[0]);

        }

        if(e.getSource()== einstellungen){

            auswahl();

        }

        if(e.getSource()== ende){
            System.exit(0);
        }



    }

    public static void fenster(){
        JFrame fenster = new JFrame();
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenster.setSize(650,350);
        fenster.setVisible(true);
        fenster.add(new gui());
    }

    public static void auswahl(){

    }

}
