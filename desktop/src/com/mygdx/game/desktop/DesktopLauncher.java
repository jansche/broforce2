package com.mygdx.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Broforce 2";
		cfg.height = 800;
		cfg.width = 1500;
		cfg.addIcon("symbol.png", Files.FileType.Internal);
		new LwjglApplication(new MyGdxGame(), cfg);
	}
}
